<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

use org\bovigo\vfs\vfsStream;
use Shikachuu\Picolog\Output\StreamOutput;

it('can create the file', function () {
    $fileName = '/test.txt';
    $root = vfsStream::setup('root', 0777, [$fileName => '']);

    $object = new StreamOutput($root->url() . $fileName);

    $reflector = new ReflectionClass($object);

    $stream = $reflector->getProperty('stream');
    $stream->setAccessible(true);

    expect($stream->getValue($object))->not()->toBeFalse()->toBeResource();
});

it('can write to the file', function () {
    $fileName = '/test.txt';
    $root = vfsStream::setup('root', 0777, [$fileName => '']);

    $object = new StreamOutput($root->url() . $fileName);
    $object->write('test');

    $content = file_get_contents($root->url() . $fileName);

    expect($content)->not()->toBeFalse();

    expect($content)->toBe('test');
});

<?php

/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

use Shikachuu\Picolog\Formatter\LogfmtFormatter;

it('can prepare a log array', function (array $target, array $expected) {
    $logfmt = new LogfmtFormatter();

    $reflector = new \ReflectionClass($logfmt);

    $prepareLogArray = $reflector->getMethod('prepareLogArray');
    $prepareLogArray->setAccessible(true);

    expect($prepareLogArray->invokeArgs($logfmt, [$target]))->toBe($expected);
})->with([
    [['asd' => 1, 'float' => 4321.123], ['asd=1', 'float=4321.123']],
    [['asd', 1], ['asd=true', '1=true']],
    [['boolf' => false, 'boolt' => true], ['boolf=false', 'boolt=true']],
    [["asd \t" => 'asd'], ['asd__="asd"']],
    [['asd' => ['asd' => 'asd']], ['asd.asd="asd"']],
]);

it('can parse exceptions', function () {
    $logfmt = new LogfmtFormatter();
    $reflector = new \ReflectionClass($logfmt);

    $parseException = $reflector->getMethod('parseException');
    $parseException->setAccessible(true);

    $exception = new \Exception('Test', 1);

    $result = $parseException->invokeArgs($logfmt, [$exception]);

    expect($result[0])->toBe('exception.message="Test"');

    expect($result[1])->toBe('exception.code=1');
});

it('can convert an array', function () {
    expect((new LogfmtFormatter())(['asd', 1]))->toBe('asd=true 1=true');
});

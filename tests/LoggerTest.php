<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use org\bovigo\vfs\vfsStream;
use Psr\Log\LogLevel;
use Shikachuu\Picolog\Logger;

it('validates log levels correctly', function (string $logLevel, bool $shouldThrowException) {
    $fileName = '/test.txt';
    $root = vfsStream::setup('root', 0777, [$fileName => '']);

    $logger = new Logger('test', $root->url() . $fileName);

    $reflector = new ReflectionClass($logger);
    $validateLogLevel = $reflector->getMethod('validateLogLevel');
    $validateLogLevel->setAccessible(true);

    if ($shouldThrowException) {
        $this->expectExceptionMessage('Invalid log level provided');

        $validateLogLevel->invokeArgs($logger, [$logLevel]);
    } else {
        $validateLogLevel->invokeArgs($logger, [$logLevel]);

        expect(strtolower($logLevel))->toBeIn([
            LogLevel::ALERT,
            LogLevel::DEBUG,
            LogLevel::CRITICAL,
            LogLevel::EMERGENCY,
            LogLevel::ERROR,
            LogLevel::NOTICE,
            LogLevel::WARNING,
            LogLevel::INFO,
        ]);
    }
})
    ->with([
        ['alErT', false],
        ['car', true],
        ['info', false],
        ['EMERGENCY', false],
        ['yeet', true],
    ]);

test('json logs', function () {
    $fileName = '/test.txt';
    $root = vfsStream::setup('root', 0777, [$fileName => '']);

    $logger = new Logger('test', $root->url() . $fileName, 'Europe/London', true);

    $logger->log(LogLevel::INFO, 'test', ['asd' => true]);

    $content = file_get_contents($root->url() . $fileName);

    expect($content)->toBeJson();

    $unmarshalledLogMessage = json_decode($content, true);

    expect($unmarshalledLogMessage)->toHaveKeys([
        'level',
        'ts',
        'logger_name',
        'msg',
        'asd',
    ]);

    expect($unmarshalledLogMessage['logger_name'])->toBe('test');

    expect($unmarshalledLogMessage['level'])->toBe('info');

    expect($unmarshalledLogMessage['msg'])->toBe('test');

    expect(DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s.uP', $unmarshalledLogMessage['ts']))
        ->toBeInstanceOf(DateTimeImmutable::class);

    expect($unmarshalledLogMessage['asd'])->toBeTrue();
});

test('logfmt logs', function () {
    $fileName = '/test.txt';
    $root = vfsStream::setup('root', 0777, [$fileName => '']);

    $logger = new Logger('test', $root->url() . $fileName);

    $logger->log(LogLevel::INFO, 'test', ['asd' => 12]);

    $content = str_replace(PHP_EOL, '', file_get_contents($root->url() . $fileName));

    $semiParsedContent = explode(' ', $content);

    expect($semiParsedContent)->toBeArray()->toHaveCount(5);

    expect(array_search('level="info"', $semiParsedContent))->toBe(0);

    expect(
        DateTimeImmutable::createFromFormat(
            'Y-m-d\TH:i:s.uP',
            trim(explode('=', $semiParsedContent[1])[1], '"'),
        ),
    )->toBeInstanceOf(DateTimeImmutable::class);

    expect(array_search('logger_name="test"', $semiParsedContent))->toBe(2);

    expect(array_search('msg="test"', $semiParsedContent))->toBe(3);
    expect(array_search('asd=12', $semiParsedContent))->toBe(4);
});

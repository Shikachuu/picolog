<?php

declare(strict_types=1);

namespace Shikachuu\Picolog\Formatter;

class LogfmtFormatter
{
    /**
     * Marshall the given array to the logfmt format.
     * @see https://www.brandur.org/logfmt
     * @see https://pkg.go.dev/github.com/go-logfmt/logfmt
     * @param array $logFields
     * @return string Marshalled logfmt string line.
     */
    public function __invoke(array $logFields): string
    {
        $parsedLogArray = $this->prepareLogArray($logFields);
        return join(' ', $parsedLogArray);
    }

    /**
     * Parses the given array fields to log message fragments.
     * @param array $logFields
     * @return array<string> Logfmt log message fragments.
     */
    private function prepareLogArray(array $logFields): array
    {
        $parsedKeyValPairs = [];
        foreach ($logFields as $key => $value) {
            if (is_int($key) && is_scalar($value) && is_bool($value) === false) {
                $parsedKeyValPairs[] = sprintf('%s=true', $value);
                continue;
            }

            if (is_string($value)) {
                $value = sprintf('"%s"', $value);
            }

            if (is_bool($value)) {
                $value = $value ? 'true' : 'false';
            }

            if (is_string($key)) {
                $key = str_replace([' ', "\t", PHP_EOL], '_', $key);
            }

            if (is_string($key) && strtolower($key) === 'exception' && $value instanceof \Exception) {
                $this->parseException($value);
                continue;
            }

            if (is_array($value) || is_object($value)) {
                $subArray = $this->prepareLogArray((array)$value);
                foreach ($subArray as $subValue) {
                    $parsedKeyValPairs[] = sprintf('%s.%s', $key, $subValue);
                }
                continue;
            }
            $parsedKeyValPairs[] = sprintf('%s=%s', $key, $value);
        }
        return $parsedKeyValPairs;
    }

    /**
     * Parses an exception to log line parts.
     * @param \Exception $exception
     * @return array<string> Parsed log parts as array.
     */
    private function parseException(\Exception $exception): array
    {
        return [
            sprintf('exception.message="%s"', $exception->getMessage()),
            sprintf('exception.code=%d', $exception->getCode()),
            sprintf('exception.line="%s:%d"', $exception->getFile(), $exception->getLine()),
            sprintf('exception.trace="%s"', $exception->getTraceAsString()),
        ];
    }
}

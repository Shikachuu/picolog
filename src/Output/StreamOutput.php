<?php

declare(strict_types=1);

namespace Shikachuu\Picolog\Output;

class StreamOutput
{
    /** @var resource $stream */
    private $stream;

    public function __construct(string $streamName)
    {
        $stream = fopen($streamName, 'a+');
        if (is_resource($stream) === false) {
            throw new \InvalidArgumentException('Failed to open file, invalid path or permission.');
        }
        $this->stream = $stream;
    }

    /**
     * Write the give message to its underlying storage.
     * @param string $message Log message that should be written.
     */
    public function write(string $message): void
    {
        fwrite($this->stream, $message);
    }

    /**
     * @codeCoverageIgnore
     */
    public function __destruct()
    {
        fclose($this->stream);
    }
}

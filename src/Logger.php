<?php

declare(strict_types=1);

namespace Shikachuu\Picolog;

use DateTimeZone;
use Psr\Log\{AbstractLogger, InvalidArgumentException, LoggerInterface, LogLevel};
use JsonException;
use Shikachuu\Picolog\Formatter\LogfmtFormatter;
use Shikachuu\Picolog\Output\StreamOutput;

class Logger extends AbstractLogger implements LoggerInterface
{
    protected string $name = 'default';

    protected DateTimeZone $timeZone;

    protected StreamOutput $output;

    protected bool $useJson;

    private const TIME_FORMAT = 'Y-m-d\TH:i:s.uP';

    public function __construct(
        string $name,
        string $streamUrl,
        string $timeZone = 'Europe/London',
        bool $useJson = false,
    ) {
        $this->name = $name;
        $this->useJson = $useJson;
        $this->timeZone = new DateTimeZone($timeZone);
        $this->output = new StreamOutput($streamUrl);
    }

    /**
     * Validates loglevel
     * @param string $level
     * @throws InvalidArgumentException
     */
    private static function validateLogLevel(string $level): void
    {
        $logLevels = [
            LogLevel::ALERT,
            LogLevel::DEBUG,
            LogLevel::CRITICAL,
            LogLevel::EMERGENCY,
            LogLevel::ERROR,
            LogLevel::NOTICE,
            LogLevel::WARNING,
            LogLevel::INFO,
        ];
        if (in_array(strtolower($level), $logLevels, true) === false) {
            throw new InvalidArgumentException('Invalid log level provided');
        }
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level Level of the log message
     * @param string|\Stringable $message Log message
     * @param array $context Context that will be parsed and included to the message
     * @return void
     * @throws JsonException Thrown when invalid date or log level provided
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        self::validateLogLevel((string)$level);

        try {
            $timestamp = new \DateTimeImmutable('now', $this->timeZone);
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage());
        }

        $preparedMessage = [
            'level' => strtolower((string)$level),
            'ts' => $timestamp->format(self::TIME_FORMAT),
            'logger_name' => $this->name,
            'msg' => $message,
        ];

        if ($this->useJson) {
            $formattedMessage = json_encode(
                $preparedMessage + $context,
                JSON_UNESCAPED_UNICODE | JSON_THROW_ON_ERROR,
            );
        } else {
            $formatter = new LogfmtFormatter();
            $formattedMessage = $formatter($preparedMessage + $context);
        }

        $this->output->write($formattedMessage . "\n");
    }
}
